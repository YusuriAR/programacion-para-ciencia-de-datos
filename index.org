# Local IspellDict: es
#+SPDX-FileCopyrightText: 2020 Adolfo De Unánue <https://nanounanue.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+STARTUP: showeverything
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="./css/index.css" />
#+TITLE: Programación para Ciencia de Datos
#+SUBTITLE: Semestre Otoño 2020
#+AUTHOR: Adolfo De Unánue
#+OPTIONS: html-style:nil
#+OPTIONS: toc:nil
#+KEYWORDS:
#+DESCRIPTION:


:COURSE_INFO:
#+MACRO: COURSE Programación para Ciencia de Datos
#+MACRO: COURSE_NUM
#+MACRO: COURSE_DATES Ma/Jue, 09:00-10:30am
#+MACRO: COURSE_LOC RH B5 (Río Hondo)
#+MACRO: SEMESTER Otoño 2020
#+MACRO: OFFICE_HOURS Jue y Vie 12:00-13:15pm
#+MACRO: REPO_LINK [[https://gitlab.com/nanounanue-clases/programacion-para-ciencia-de-datos][gitlab]]
#+MACRO: WEIGHT_PROPOSAL 10%
#+MACRO: WEIGHT_FINAL_REPORT 20%
#+MACRO: WEIGHT_PRESENTATION 15%
#+MACRO: WEIGHT_DEEP_DIVE 10%
#+MACRO: WEIGHT_EXERCISES 5%
#+MACRO: WEIGHT_CLASS_PARTICIPATION 20%
#+MACRO: WEIGHT_ASSIGNMENT 20%
#+MACRO: WEIGHT_PEERREVIEW 10%
#+MACRO: WEIGHT_FEEDBACK 5%
#+MACRO: WEIGHT_ACTIVITIES 10%
:END:


* Descripción del curso                                              :ignore:
#+INCLUDE: "pcd.org::#Syllabus/Course Description" :only-contents t

* Profesor

| {{{AUTHOR}}} |
|--------|
| [[file:assets/figuras/fotos/Adolfo.jpg]] |


* Material
  :PROPERTIES:
  :CUSTOM_ID: presentations
  :END:

- [[file:texts/syllabus.org][Syllabus]]
- [[file:texts/setup.org][Setup]]
- 06/08  [[file:lectures/01-intro.org][01 - Introducción]] ([[file:lectures/01-intro.html?print-pdf][PDF]])
- 11/08  [[file:lectures/02-cli.org][02 - Línea de comandos]] ([[file:lectures/02-cli.html?print-pdf][PDF]])
- 13/08  [[file:lectures/03-cli.org][03 - Línea de comandos]] ([[file:lectures/03-cli.html?print-pdf][PDF]])
- 18/08 /ibid/
- 20/08  [[file:lectures/04-cli.org][04 - Línea de comandos]] ([[file:lectures/04-cli.html?print-pdf][PDF]])
- 25/08  [[file:lectures/05-cli.org][05 - Línea de comandos]] ([[file:lectures/05-cli.html?print-pdf][PDF]])
- 27/08  [[file:lectures/06-cli.org][06 - Línea de comandos]] ([[file:lectures/06-cli.html?print-pdf][PDF]])

* Calendario tentativo
#+INCLUDE: "pcd_2020.org::#Syllabus/Class Schedule" :only-contents t


* Evaluación

#+INCLUDE: "pcd.org::#Syllabus/Grading" :only-contents t

* ¿Cómo contribuir?

Como en cualquier proyecto de *GitLab*, puedes abrir [[https://docs.gitlab.com/ee/user/project/issues/create_new_issue.html][issues]]
para reportar /bugs/ o sugerir mejoras, idealmente usando [[https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html][merge requests]].

* Agradecimientos

Las presentaciones fueron producidas  con el /framework/ [[https://revealjs.com/][reveal.js]].

El código fuente está hecho en =GNU/Emacs= src_emacs-lisp[:results raw
:exports results :eval never-export]{(insert emacs-version)}27.0.50  usando =org-mode= version src_emacs-lisp[:results raw
:exports results :eval never-export]{(org-version)} 9.3.6.

* Código fuente y licencias
#+MACRO: gitlablink [[https://gitlab.com/oer/misc][$1]]
# +INCLUDE: "source-code-licenses.org"
