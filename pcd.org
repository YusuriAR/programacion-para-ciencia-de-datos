#+TITLE:     Programación para Ciencia de Datos
#+AUTHOR:    Adolfo De Unánue
#+EMAIL:     unanue@itam.mx
#+DESCRIPTION:
#+CATEGORY: COURSE
#+STARTUP: overview
#+STARTUP: hidestars
#+LANGUAGE: es


* Cursos
** Otoño 2019
** Otoño 2020

* Syllabus
** Descripción del curso
:PROPERTIES:
:CUSTOM_ID: Syllabus/Course Description
:END:


Este curso es una /introducción/ a las herramientas computacionales
necesarias para poder resolver problemas de Ciencia de datos,
/Machine Learning/ o Inteligencia Artificial de una manera eficiente y
cómoda

Este curso le enseñará a los estudiantes

1) Sentirse cómodo con las herramientas necesarias para Ciencia de Datos.
2) Obtener, manipular y almacenar datos que facilitarán los flujos de
   Ciencia de Datos.
3) Colaborar con otros Científicos de Datos en un proyecto.
4) Contestar preguntas de Ciencia de Datos usando la herramienta apropiada
5) Reproducibilidad del flujo de Ciencia de Datos.

Este es un curso /hands-on/ en el que los estudiantes usarán la
computadora para implementar la solución a varios problemas de Ciencia
de datos.


** Prerrequisitos
:PROPERTIES:
:CUSTOM_ID: Syllabus/Course Prerequisites
:END:

- Tener acceso a una /laptop/ o algún dispositivo de cómputo

- Experiencia en /cualquier/ lenguage de programación es recomedada,
  pero no necesaria.


** TODO Objetivos de Aprendizaje del Estudiante
:PROPERTIES:
:CUSTOM_ID: Syllabus/Student Learning Objectives
:END:
:LOGBOOK:
- State "TODO"       from              [2020-08-06 Thu 00:50]
:END:
Al finalizar este curso, el estudiante debería de ser capaz de:

#+ATTR_REVEAL: :frag (appear)
- ...

** Material del Curso
:PROPERTIES:
:CUSTOM_ID: Syllabus/Course Material
:END:
El material de este curso es trabajo en progreso. El calendario es
sujeto de cambio basado en los inteereses  de la clase y su
progreso. Adicionalmente, podríamos tener invitados especiales a
platicar o compartir su experiencia.

Si tienes un tema en particular o invitados que te gustaría ver, por
favor cuéntamelo.

- Documentos :: Los /Handouts/, lecturas y tareas se colocarán al repositorio.

#+REVEAL_HTML: <span hidden>
#+ATTR_LATEX: :options [style=exampledefault, frametitle={Note}]
#+BEGIN_mdframed
      - Por favor, instala =pyenv= (sigue las  [[https://github.com/pyenv/pyenv-installer][instrucciones]]). En esta clase usaremos la versión
        =Python 3.7= . Te recomendamos crear un ambiente virtual llamado
        =pcd=. (instrucciones [[https://stackoverflow.com/a/46073367/754176][aquí]])
#+END_mdframed
#+REVEAL_HTML: </span>



** Evaluación
:PROPERTIES:
:CUSTOM_ID: Syllabus/Grading
:END:

- Tareas y mini-exámenes cubriendo los temas vistos en clases.
- *NO* habrá examen final. En su lugar cada equipo deberá entregar un
  reporte y una presentación oral al final del semestre.


Los componentes de la calificación incluyen:

| Componente                                                         | Valor                    |
|--------------------------------------------------------------------+--------------------------|
| /Peer review/ de tres  proyectos                                     | {{{WEIGHT_PEERREVIEW}}}         |
| Presentación final de los resultados orientada a /stakeholders/      | {{{WEIGHT_PRESENTATION}}}       |
| Reporte final del proyecto escrito y código                        | {{{WEIGHT_FINAL_REPORT}}}        |
| Participación en clase durante la discusiones y las presentaciones | {{{WEIGHT_CLASS_PARTICIPATION}}} |
| Tareas                                                             | {{{WEIGHT_ASSIGNMENT}}}         |
| Enviar semanalmente las formas de /check-in/ y /feedback/              | {{{WEIGHT_FEEDBACK}}}           |
| Participación en actividades                                       | {{{WEIGHT_ACTIVITIES}}}         |

** Políticas del curso
:PROPERTIES:
:CUSTOM_ID: Syllabus/Specific Course Policies
:END:


* COMMENT Variables locales
# Local Variables:
# mode: org
# ispell-local-dictionary: "spanish"
# org-export-allow-bind-keywords: t
# org-use-property-inheritance: t
# org-time-stamp-custom-formats: ("<%a, %b %d, %Y>" . "<%a, %b %d, %Y %l:%M%p>")
# org-latex-active-timestamp-format: "%s"
# org-latex-inactive-timestamp-format: "%s"
# org-image-actual-width: nil
# End:
