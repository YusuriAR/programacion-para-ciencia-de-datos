#+TITLE: Línea de comandos
#+DATE: Agosto 11 de 2020

#+LANGUAGE: es

#+PROPERTY: header-args :eval never-export
#+STARTUP: latexpreview
#+STARTUP: overview
#+STARTUP: hideblocks

#+EXCLUDE_TAGS: slidesonly teachersonly noexport homework appendix

#+OPTIONS: TeX:t LaTeX:t
#+OPTIONS: d:(not "TEACHERONLY" "ZOOM" "LOGBOOK")
#+OPTIONS: html-style:nil

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="/programacion-para-ciencia-de-datos/css/org-solarized-light.css" />
#+OPTIONS: email:nil num:nil author:nil tags:nil timestamp:nil toc:nil todo:t
#+KEYWORDS:
#+DESCRIPTION:


* La computadora


Las computadoras sólo hacen cuatro cosas:

- Ejecutan programas
- Almacenan datos
- Se comunican entre sí para hacer las cosas recién mencionadas.
- Interactúan con nosotros.
  - La interacción puede ser gráfica (como están acostumbrados)
    conocida también como *GUI* (/Graphical User Interface/) vía el
    ratón u otro periférico, o desde la línea de comandos, llamada
    como *CLI* (/Command Line Interface/).

*  El /shell/                                                    :handbookonly:

El =shell= de Unix (en su caso particular es un =shell= de
=GNU/Linux=), es más viejo que todos nosotros. Y el hecho de que siga
activo, y en uso, se debe a que es una de las invenciones humanas más
exitosas para usar la computadora de manera eficiente.

A manera de resumen, el =shell= puede hacer lo siguiente:

- Un intérprete interactivo: lee comandos, encuentra los programas
  correspondientes, los ejecuta y despliega la salida.
  - Esto se conoce como *REPL*: /Read, Evaluate, Print, Loop/
- La salida puede ser redireccionada a otro lugar además de la
  pantalla. (Usando =>= y =<=).
- Una cosa muy poderosa (y en la que está basada --como casi todo lo
  actual--) es combinar comandos que son muy básicos (sólo hacen una
  sola cosa) con otros para hacer cosas más complicadas (esto es con
  un *pipe* =|=).
- Mantiene un histórico que permite rejecutar cosas del pasado.
- La información es guardada jerárquicamente en carpetas o directorios.
- Existen comandos para hacer búsquedas dentro de archivos (=grep=) o
  para buscar archivos (=find=) que combinados pueden ser muy
  poderosos.
  - Se puede hacer *data analysis* solamente con estos comandos, así
    de poderosos son.
- Las ejecuciones pueden ser pausadas, ejecutadas en el *fondo* o en
  máquinas remotas.
- Además es posible definir variables para usarse por otros programas.
- El =shell= cuenta con todo un lenguaje de programación, lo que
  permite ejecutar cosas en *bucles*, *condicionales*, y cosas
  en paralelo.

* ¿Por qué?

En muchas ocasiones, se verán en la necesidad de responder muy rápido y
en una etapa muy temprana del proceso de *big data* . Las peticiones
regularmente serán cosas muy sencillas, como estadística univariable y es
aquí donde es posible responder con las herramientas mágicas de UNIX.


* Línea de comandos

La línea de comandos es lo que estará entre nosotros y la computadora
casi todo el tiempo en este curso. De hecho, una lectura obligada[fn:4] es
[[https://www.amazon.com/Beginning-Was-Command-Line-ebook/dp/B0011GA08E/ref=tmm_kin_swatch_0?_encoding=UTF8&qid=&sr=][In the beginning...was de command line]]
de *Neal Stephenson*, el escritor de
[[https://play.google.com/store/books/details/Neal_Stephenson_Cryptonomicon?id=dwAwSzmTHNEC][*Criptonomicon*]][fn:5].


La *CLI* es otro programa más de la computadora y su función es
ejecutar otros comandos. El más popular es =bash=, que es un acrónimo
de *Bourne again shell*. Aunque en esta clase también usaremos =zsh=[fn:17].


* ¡Ten cuidado!                                                :handbookonly:

La primera regla de la línea de comandos es: /ten cuidado con lo que
deseas, por que  se te va a cumplir/. La computadora hará exactamente lo
que le digas que haga, pero recuerda que los humanos
tienen dificultadas para expresarse en /lenguaje de computadoras/.

Esta dificultad puede ser muy peligrosa, sobre todo si ejecutas
programas como =rm= (/borrar/) o =mv= (/mover/).


* Creando archivos de prueba                                   :handbookonly:

Puedes crear archivos /dummy/ para este curso usando el comando
=touch=:

#+begin_src shell
touch space\ bars\ .txt
#+end_src

Nota que usamos el caracter =\= para indicar que queremos un espacio
en el nombre de nuestro archivo. Si no lo incluyes

#+begin_src shell
touch space bars .txt
#+end_src

... la computadora creará tres archivos separados: =space=, =bars=, and =.txt=[fn:18].


* Archivos y directorios

La computadora guarda la información de una manera ordenada. El
sistema encargado de esto es el =file system=, el cual es básicamente un árbol
de información[fn:19] que guarda los datos en
una abstracción que llamamos *archivos* y ordena los archivos en
*carpetas* o *directorios*, los cuales a su vez pueden contener otros
*directorios*.

#+begin_info
*TODO* en los sistemas operativos =*nix= (como Unix, GNU/Linux,
 FreeBSD, MacOS, etc)  es un /archivo/.
#+end_info


Muchos de los comandos del *CLI* o =shell= tienen que ver con la
manipulación del =file system=.

* Filosofía UNIX

Creada originalmente en 1978 por *Doug McIlroy*, la filosofía de UNIX es
un acercamiento al diseño de software que enaltece el software modular
y minimalista.

Han existido varias adaptaciones[fn:16], pero la que más me gusta es la de
*Peter H. Salus*,

Es importante tener estos principios en mente, ya que ayuda a enmarcar
los conceptos que siguen.

#+begin_quote
- Escribe programas que hagan una cosa y que la hagan bien
- Escribe programas que puedan trabajar en conjunto
- Escribe programas que puedan manipular /streams/ de texto, ya que
  el texto es la interfaz universal.
#+end_quote




* Usando el /shell/

La primera prueba de fuego, es encontrar la *terminal*, pero una vez que
lo hayas logrado, lo primero que verás el /prompt/:

#+begin_example
usuario:~$
#+end_example

(o algo muy parecido)

Intentemos algo simple, demos la instrucción a la computadora para que
repita lo que le estamos escribiendo, es decir, que haga /eco/

#+begin_src shell
echo "¡Hola! Bienvenido al curso"
#+end_src

#+RESULTS:
: ¡Hola! Bienvenido al curso


* Conocer los alrededores                                               :cli:

** Navegación en la terminal
Moverse rápidamente en la *CLI* es de vital importancia. Teclea en tu /terminal/

#+begin_src shell
Anita lava la tina
#+end_src

Y ahora intenta lo siguiente:

- =Ctrl + a= :: Inicio de la línea
- =Ctrl + e= :: Fin de la línea
- =Ctrl + r= :: Buscar hacia atrás[fn:6]
- =Ctrl + b= :: Mueve el cursor hacia atrás una letra a la vez
- =Alt + b= :: Mueve el cursor hacia atrás una palabra a la vez
- =Ctrl + f= :: Mueve el cursor hacia adelante una letra a la vez
- =Alt + f= :: Mueve el cursor hacia adelante una palabra a la vez

#+REVEAL: split

- =Ctrl + k= :: Elimina el resto de la línea (en realidad corta y pone en el búfer circular)
- =Ctrl + y= :: Pega la último del búfer.
- =Alt  + y=  :: Recorre el búfer circular.
- =Ctrl + d= :: Cierra la terminal
- =Ctrl + z= :: Manda a /background/ el programa que se está ejecutando
- =Ctrl + c= :: Intenta cancelar
- =Ctrl + l= :: Limpia la pantalla

#+REVEAL: split

#+begin_warning
Estas combinaciones de teclas (/keybindings/) son universales. Te
recomiendo que las practiques y configures tus otras herramientas con
estos mismas combinaciones, por ejemplo [[https://support.rstudio.com/hc/en-us/articles/210928128-Emacs-Editor-Keybindings][RStudio]] ó [[https://jupyterlab.readthedocs.io/en/stable/user/interface.html?highlight=emacs#keyboard-shortcuts][JupyterLab]] .
#+end_warning


#+begin_question
¿Qué hacen las siguientes combinaciones?

- =Alt  + t=
- =Alt  + d=
- =Ctrl + j=
- =Alt + 2 Alt + b=
#+end_question

Para tener más información sobre los /bindings/ consulta [[https://catonmat.net/bash-emacs-editing-mode-cheat-sheet][aquí]].

** ¿Quién soy?                                                :handbookonly:

#+begin_src shell
whoami
#+end_src

** ¿Quién está conmigo?                                       :handbookonly:

#+begin_src shell
who
#+end_src

** ¿Dónde estoy?
Imprime el nombre del /directorio/ actual

#+begin_src shell
pwd
#+end_src

Cambia el directorio un nivel arriba (a el directorio /padre/ [fn:2])

#+begin_src shell
cd ..
#+end_src

Si quieres regresar al directorio anterior

#+begin_src shell
cd -
#+end_src

** ¿Qué día es hoy?

#+begin_src shell
date
#+end_src

** Hogar dulce, hogar

Cambia el directorio =$HOME= (tu directorio) utilizando =~=

#+begin_src shell
cd ~
#+end_src

o bien, no pasando ningún argumento

#+begin_src shell
cd
#+end_src

** ¿Qué hay en mi directorio (/folder/) ?
=ls= Lista los contenidos (archivos y directorios) en el
directorio actual, pero no los archivos /ocultos/.

#+begin_src shell
ls
#+end_src

Lista los contenidos en formato /largo/ (=-l=), muestra
  el tamańo de los archivos, fecha de último cambio y permisos

#+begin_src shell
ls -l
#+end_src

Lista los contendios en el directorio actual y todos los
  sub-directorios en una estructura de /árbol/

#+begin_src shell
tree
#+end_src

Límita la expansión del /árbol/ a dos niveles
#+begin_src shell
tree -L 2
#+end_src

Muestra los archivosshows file sizes (=-s=) in
  human-readable format (=-h=)
#+begin_src shell
tree -hs
#+end_src

** ¿Qué hay en mi archivo?
Muestra el principio (/head/) del archivo, =-n= especifica el número de líneas (=10=).

#+begin_src shell
head -n10 $f
#+end_src

Muestra la final (/tail/) del archivo.
#+begin_src shell
tail -n10 $f
#+end_src

Muestra la parte final del archivo
  cada segundo (usando =watch=)

#+begin_src shell
tail -n10 $f | watch -n1
#+end_src


"seguir" (/follows/) (=-f=) la parte final del archivo, cada vez que hay cambios

#+begin_src shell
tail -f -n10 $f
#+end_src

#+begin_info
Seguir archivos es útil cuando estás ejecutando un programa que guarda
    información a un archivo, por ejemplo un /log/
#+end_info

Cuenta las palabras, caracteres y líneas de un archivo

#+begin_src shell
wc $f
#+end_src

** ¿Dónde está mi archivo?

Encuentra el archivo por nombre

#+begin_src shell
find -name "<lost_file_name>" -type f
#+end_src

Encuentra directorios por nombre

#+begin_src shell
find -name "<lost_dir_name>" -type d
#+end_src

**  /Caveats/ con =git=                                       :handbookonly:

Mover archivos puede confundir a =git=. Si estás trabajando con
archivos en =git= usa lo siguiente:

#+begin_src shell
# Para mover o renombrar
git mv /source/path/$move_me /destination/path/$move_me

# Para eliminar
git rm $remove_me
#+end_src

** Practiquemos un poco                                       :handbookonly:
:PROPERTIES:
:DIR:      ./images
:END:


Enciende la máquina virtual

#+begin_src shell
vagrant up
#+end_src

Conéctate a la máquina virtual con =vagrant=

#+begin_src shell
vagrant ssh
#+end_src

Teclea =whoami= y luego presiona *enter*. Este comando te dice que
usuario eres.

#+begin_comment
Tu usuario, es un usuario normal. Existe otro usuario  llamado =root=
el cual un *superusuario*, el tiene poderes para modificar
/todo/. Esto es peligroso, no caigas en la tentación de resolver cosas
con ese usuario
#+end_comment

Teclea =cd /=

Para saber donde estamos en el =file system= usamos =pwd= (de /print working directory/).

#+begin_info
Estamos posicionados en la raíz del árbol del sistema, el cual es
simbolizada como =/=.
#+end_info

Para ver el listado de un directorio usamos =ls=

#+begin_info
Ahora estás observando la estructura de directorios de =/=.
#+end_info

Los comandos (como =ls=) pueden tener modificadores o *banderas* (/flags/),
las cuales modifican (vaya sorpresa) el comportamiento por omisión
del comando. Intenta lo siguiente:  =ls -l=,  =ls -a=, =ls -la=,  =ls -lh=, =ls
-lha=. Discute con tu compañero junto a ti las diferencias entre las banderas.

Para obtener ayuda puedes utilizar =man= (de /manual/) y el nombre del
comando.

#+begin_question
¿Cómo puedes aprender que hace =ls=?
#+end_question

Puedes buscar dentro de =man page= para =ls= (o de cualquier otro
manual) si tecleas =/= y escribiendo la palabra que buscas, luego
presiona =enter= para iniciar la búsqueda. Esto te mostrará la
primera palabra que satisfaga el criterio de búsqueda. =n= te
mostrará la siguiente palabra. =q= te saca del =man page=.

Busca la bandera para ordenar (/sort/) el listado de directorios por
tamaño.

Muestra el listado de archivos de manera ordenada por archivo.

Otro comando muy útil (aunque no lo parecerá ahorita) es =echo=.

Las variables de sistema (es decir globales en tu sesión) se pueden
obtener con el comando =env=. En particular presta atención a
=HOME=, =USER= y =PWD=.

Para evaluar la variable podemos usar el signo de moneda =$=,

Imprime las variables con =echo=, e.g.

#+begin_src shell
echo $USER
#+end_src

#+begin_question
¿Qué son las otras variables =HOME=, =PWD=?
#+end_question


El comando =cd= permite cambiar de directorios (¿Adivinas de donde
viene el nombre del comando?) La sintáxis es =cd
nombre_directorio=.

#+begin_question
¿Cuál es la diferencia si ejecutas =ls -la= en ese directorio?
#+end_question

#+begin_info
Las dos líneas de hasta arriba son =.= y =..= las cuales
significan *este directorio* (=.=) y el directorio padre (=..=)
respectivamente. Los puedes usar para navegar (i.e. moverte con
=cd=)
#+end_info

#+begin_question
¿Puedes regresar a raíz?
#+end_question

#+begin_question
En raíz (=/=) ¿Qué pasa si ejecutas =cd $HOME=?
#+end_question

#+begin_info
Otras maneras de llegar a tu =$HOME= son =cd ~= y =cd= (sin argumento).
#+end_info

Verifica que estés en tu directorio (¿Qué comando usarias?) Si no
estás ahí, ve a él.

Para crear un directorio existe el comando =mkdir= que recibe como
parámetro un nombre de archivo.

Crea la carpeta =test=. Entra a ella. ¿Qué hay dentro de ella?

Vamos a crear un archivo de texto, para esto usaremos =nano=[fn:3]. Por el momento
teclea

#+begin_src shell
nano hola.txt
#+end_src

y presiona enter.

#+CAPTION: Editor =nano=, mostrando el archivo recién creado =hola.txt= .
#+ATTR_ORG: :width 600 :height 600
#+ATTR_HTML: :width 800px :height 600px
#+ATTR_LATEX: :height 5cm :width 8cm
[[file:./images/screenshot-01.png]]


Teclea en =nano=

#+begin_src bash
¡Hola Mundo!
#+end_src

 y luego presiona la siguiente combinación de teclas: =Ctrl+O= [fn:7]para
 guardar el archivo (Te va a  preguntar /dónde/ guardarlo).


=Ctrl+X= para salir de =nano=. Esto te devolverá a la  línea de
comandos.

Verifica que esté el archivo.


Para ver el contenido de un archivo tienes varias opciones (además
de abrir =nano=): =cat=, =more=, =less=

#+begin_src shell
cat hola.txt
#+end_src

Para borrar usamos el comando =rm= (de /remove/).

Borra el archivo =hola.txt=.

#+begin_question
¿Cómo crees que se borraría un directorio?

¿Puedes borrar el directorio =test=? ¿Qué falla? ¿De dónde
puedes obtener ayuda?
#+end_question

Crea otra carpeta llamada =tmp=, crea un archivo =copiame.txt= con
=nano=, escribe en él:

#+begin_src shell
Por favor cópiame
#+end_src

Averigua que hacen los comandos =cp= y =mv=.

Copia el archivo a uno nuevo que se llame =copiado.txt=.

Borra =copiame.txt=.

Modifica =copiado.txt=, en la última línea pon

#+begin_src shell
¡Listo!
#+end_src

Renombra =copiado.txt=  a =copiame.txt=.

Por último borra toda la carpeta =tmp=.

Desconéctate  de la máquina virtual con =Ctrl+D= y luego "apaga" la
máquina virtual

#+begin_src shell
vagrant halt
#+end_src

** /Wildcards/: Globbing

La línea de comandos permite usar comodines (/wildcards/)[fn:11]
para encontrar  archivos:

El primer /glob/ que veremos es =*=:

#+begin_src shell
echo *
#+end_src

El =shell= expandió =*= para identificarlo con todos los archivos del
directorio actual.

Obvio lo puedes usar con otros comandos, como =ls=:
Listar todos los archivos que tienen una extensión =txt=

#+begin_src shell
ls *.txt
#+end_src

Listar todos los archivos que contienen =a= en el nombre y extensión
=txt=

#+begin_src shell
ls *a*.txt
#+end_src

=*= no es el único carácter especial. =?= hace /match/ con cualquier
carácter individual.

Listar todos los archivos que tienen 5 caracteres en el nombre:

#+begin_src shell
ls ?????,txt
#+end_src


* Footnotes


[fn:19] Aunque hay varios tipos de =file systems= (=ext3=, =ext4=, =xfs=,
=bfs=, etc) que pueden utilizar modificaciones a esta estructura de datos, lo que voy a decir
aplica desde el punto de vista de usuario del /file system/ no su
especificación técnica.

[fn:18] Ve la advertencia en la sección anterior

[fn:17] =zsh= significa /Z shell/. Lo sé, es algo decepcionante.

[fn:16] Una discusión larga y detallada se encuentra en [[http://www.catb.org/~esr/writings/taoup/html/][The Art of Unix
Programming]] de *Eric Steven Raymond*.


[fn:11] También permite expresiones regulares (/regular
expressions/, a.k.a. /regexp/), pero es importante saber que *NO* son iguales. /Globs/
y /regexps/ son usadas en diferentes contextos y significan diferentes
cosas. Por ejemplo el símbolo =*=, es un modificador de cantidad en
regexp, pero expande cuando es usado como /globs/. Más adelante
veremos un ejemplo.

[fn:7] Las combinaciones de las teclas están desplegadas en la parte
inferior de la pantalla

[fn:6] Elimina el nefasto y tardado /flechita arriba/

[fn:3] Otras opciones son:  [[https://www.gnu.org/software/emacs/][=GNU Emacs=]] (un editor de textos muy
poderoso. Es el que estoy usando) o [[https://www.vim.org/][=vi=]]. No importa cual escojas,
aprende a usarlo muy bien. Recuerda, queremos disminuir el dolor.

[fn:2] En inglés es /parent directory/, no se me ocurrió otra
traducción ¿Alguna sugerencia?

[fn:1] Si no tienes una /laptop/ pero tienes una /tablet/ o inclusive
un teléfono móvil, podemos configurarlo de tal manera que te conectes
a algún servidor remoto y puedas seguir las lecciones de este curso.


[fn:4] No es de tarea, pero debería de serlo

[fn:5] Otra lectura recomendada
